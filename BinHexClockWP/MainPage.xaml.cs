﻿using System;
using System.Windows;
using System.Windows.Threading;
using BinHexClockCommon;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace BinHexClockWP
{
    public partial class MainPage
    {
        private readonly ClockController m_clock;
        private readonly DispatcherTimer m_buttonTimer;

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            m_clock = new ClockController(Timer, App.Options);

            m_buttonTimer=new DispatcherTimer();
            m_buttonTimer.Interval = new TimeSpan(0,0,0,3);
            m_buttonTimer.Tick += (sender, e) =>
                {
                    ConfigureButton.Visibility=Visibility.Collapsed;
                    m_buttonTimer.Stop();
                };
        }

        private void Timer(string strTime,string strDate)
        {
            ClockTime.Dispatcher.BeginInvoke(() => ClockTime.Text = strTime);
            if (!App.Options.ShowOnlyTime)
                ClockDate.Dispatcher.BeginInvoke(() => ClockDate.Text = strDate);
//            else
//                ClockDate.Text = String.Empty;
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            //For returning from options
            if(App.Options.ShowOnlyTime)
                ClockDate.Text = String.Empty;

            ClockBase.Text = App.Options.GetBaseAsString();

            m_clock.StartTimer();

        }

        private void Configure_Tap(object sender, GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/ConfigurePage.xaml", UriKind.Relative));
        }

        private void LayoutRoot_OnTap(object sender, GestureEventArgs e)
        {
            ConfigureButton.Visibility=Visibility.Visible;
            m_buttonTimer.Start();
        }
    }
}