﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using BinHexClockCommon;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace BinHexClockWP
{
    public partial class ConfigurePage
    {
        public ConfigurePage()
        {
            InitializeComponent();
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            ShowTimeList.SelectedIndex = (int) App.Options.ShowMinutesOrSeconds;
            ShowBaseList.SelectedIndex = (int) App.Options.ShowBase;

            ShowOnlyTimeList.SelectedIndex = App.Options.ShowOnlyTime ? 1 : 0;
        }

        private void PhoneApplicationPage_Unloaded(object sender, RoutedEventArgs e)
        {
            App.Options.ShowBase = (ShowBase)ShowBaseList.SelectedIndex;
            App.Options.ShowMinutesOrSeconds = (ShowMinutesOrSeconds) ShowTimeList.SelectedIndex;
            App.Options.ShowOnlyTime = ShowOnlyTimeList.SelectedIndex != 0;

            App.Options.Save();
        }
    }
}