﻿using System.IO.IsolatedStorage;
using BinHexClockCommon;

namespace BinHexClockWP
{
    class Settings: LocalSettings
    {
        public override void SetValue(string name, object value)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(name))
                IsolatedStorageSettings.ApplicationSettings[name] = value;
            else
                IsolatedStorageSettings.ApplicationSettings.Add(name, value);
        }

        public override object GetValue(string name, object defaultValue)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(name))
                return IsolatedStorageSettings.ApplicationSettings[name];

            return defaultValue;
        }
    }
}
