﻿using System.Threading;

namespace BinHexClockCommon
{
    public delegate void DateTimeChangedDelegate(string strTime,string strDate);

    public class ClockController
    {
        private readonly Timer m_timer;
        private int m_interval;

        private readonly ClockConverter m_clockConverter;
        private readonly DateTimeChangedDelegate m_delegate;

        public ClockController(DateTimeChangedDelegate callback,Options options)
        {
            m_delegate += callback;

            m_clockConverter=new ClockConverter(options);
            m_timer = new Timer(TimerCallback, null, 0, Timeout.Infinite);

            ConfigureInterval(options.ShowMinutesOrSeconds);
        }

        public void ConfigureInterval(ShowMinutesOrSeconds interval)
        {
            if (interval == ShowMinutesOrSeconds.Minutes)
                m_interval = 60 * 1000;   //One minute
            else
                m_interval = 1000;  //One second

            StartTimer();
        }

        public void StartTimer()
        {
            m_timer.Change(0, m_interval);
        }

        public void StopTimer()
        {
            m_timer.Change(0, Timeout.Infinite);
        }

        private void TimerCallback(object obj)
        {
            if (m_delegate != null)
                m_delegate(m_clockConverter.ConvertTime(),m_clockConverter.ConvertDate());
        }
    }
}
