﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace BinHexClockCommon
{
    
    public enum ShowMinutesOrSeconds
    {
        Seconds=0,
        Minutes=1
    }

    public enum ShowBase
    {
        Decimal=0,
        Hexadecimal=1,
        Octal=2,
        Binary=3
    }

    public class LocalSettingsParametersNames
    {
        public const string ShowTime = "ShowMinutesOrSeconds";
        public const string ShowBase = "ShowBase";
        public const string ShowOnlyTime = "ShowOnlyTime";
    }

    public abstract class LocalSettings
    {
        public abstract void SetValue(string name, object value);
        public abstract object GetValue(string name,object defaultValue);

        public static LocalSettings Instance { get; set; }
    }

    public class Options
    {
        private bool m_modified;
        private ShowBase m_showBase;
        private ShowMinutesOrSeconds m_ShowMinutesOrSeconds;
        private bool m_showOnlyTime;

        public ShowBase ShowBase
        {
            get { return m_showBase; }
            set
            {
                if(m_showBase==value)
                    return;

                m_showBase = value;
                m_modified = true;
            }
        }

        public ShowMinutesOrSeconds ShowMinutesOrSeconds
        {
            get { return m_ShowMinutesOrSeconds; }
            set
            {
                if (m_ShowMinutesOrSeconds == value)
                    return;

                m_ShowMinutesOrSeconds = value;
                m_modified = true;
            }
        }

        public bool ShowOnlyTime
        {
            get { return m_showOnlyTime; }
            set
            {
                if(m_showOnlyTime==value)
                    return;

                m_showOnlyTime = value;
                m_modified = true;
            }
        }

        public Options()
        {
            ShowBase = ShowBase.Decimal;
            ShowMinutesOrSeconds= ShowMinutesOrSeconds.Minutes;
            ShowOnlyTime = false;

            Load();
            m_modified = false;
        }

        public Options(ShowBase b, ShowMinutesOrSeconds t)
        {
            ShowBase = b;
            ShowMinutesOrSeconds = t;
            ShowOnlyTime = false;

            Load();
            m_modified = false;
        }

        ~Options()
        {
            Save();
        }

        private void Load()
        {
            ShowBase = (ShowBase)LocalSettings.Instance.GetValue(LocalSettingsParametersNames.ShowBase, ShowBase.Decimal);
            ShowMinutesOrSeconds = (ShowMinutesOrSeconds)LocalSettings.Instance.GetValue(LocalSettingsParametersNames.ShowTime, ShowMinutesOrSeconds.Minutes);
            ShowOnlyTime =(bool) LocalSettings.Instance.GetValue(LocalSettingsParametersNames.ShowOnlyTime, false);
        }

        public void Save()
        {
            if (!m_modified) 
                return;

            LocalSettings.Instance.SetValue(LocalSettingsParametersNames.ShowBase, (int)ShowBase);
            LocalSettings.Instance.SetValue(LocalSettingsParametersNames.ShowTime, (int)ShowMinutesOrSeconds);
            LocalSettings.Instance.SetValue(LocalSettingsParametersNames.ShowOnlyTime,ShowOnlyTime);
            m_modified = false;
        }

        public string GetBaseAsString()
        {
            switch (ShowBase)
            {
                case ShowBase.Hexadecimal:
                    return "HEX";
                case ShowBase.Octal:
                    return "OCT";
                case ShowBase.Binary:
                    return "BIN";
                default:
                    return "DEC";
            }
        }
    }
}
