﻿using System;
using System.Linq;
using System.Text;

namespace BinHexClockCommon
{
    public class ClockConverter
    {
        private readonly Options m_options;

        public ClockConverter(Options options)
        {
            m_options = options;
        }

        public string ConvertDate()
        {
            return ConvertToBase(DateTime.Now.ToString("d"));
        }

        public string ConvertTime()
        {
            var format = m_options.ShowMinutesOrSeconds == ShowMinutesOrSeconds.Minutes?"t":"T";

            return ConvertToBase(DateTime.Now.ToString(format));
        }

        private static char FindSeparator(string origin)
        {
            if (origin.Contains("/"))
                return '/';
            if (origin.Contains(":"))
                return ':';
            if (origin.Contains("-"))
                return '-';

            if (origin.Contains(" "))
                return ' ';

            for (var index = 0; index < origin.Length; index++)
                if (!Char.IsDigit(origin[index]))
                    return origin[index];

            return '|'; //Never here. 
        }

        private static string Resize(string numberAsString)
        {
            return numberAsString.PadLeft(numberAsString.Length > 2 ? 4 : 2, '0');
        }

        private string ConvertToBase(string origin)
        {
            var separator = FindSeparator(origin);

            var tokens = origin.Split(new[] {separator,' '});

            var baseNum=10;
            switch (m_options.ShowBase)
            {
                case ShowBase.Hexadecimal:
                    baseNum = 16;
                    break;
                case ShowBase.Octal:
                    baseNum = 8;
                    break;
                case ShowBase.Binary:
                    baseNum = 2;
                    break;
            }

            var sb = new StringBuilder();
            foreach (var token in tokens.Where(token => !String.IsNullOrEmpty(token)))
            {
                int number;
                if (Int32.TryParse(token, out number))
                {
                    if (sb.Length != 0) //Omitted only for first number
                        sb.Append(separator);
                    sb.Append(Resize(Convert.ToString(number,baseNum)));
                }
                else
                {
                    sb.Append(' ');
                    sb.Append(token);
                }
            }

            return sb.ToString();
        }
    }
}
