﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using BinHexClockCommon;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace BinHexClockW8
{
    public class FontSizeConverter:IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var width = Window.Current.Bounds.Width;
            if (width > 1400)
                return 100;
            else if (width > 1000)
                return 80;
            return 60;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage
    {
        private readonly ClockController m_clock;
        private readonly DispatcherTimer m_timer;

        public MainPage()
        {
            this.InitializeComponent();

            m_clock=new ClockController(Timer,App.Options);

            m_timer=new DispatcherTimer();
            m_timer.Interval = new TimeSpan(0,0,0,3);
            m_timer.Tick += (sender, e) =>
                {
                    ConfigureButton.Visibility = Visibility.Collapsed;
                    m_timer.Stop();
                };
        }

        async private void Timer(string strTime, string strDate)
        {
            await ClockTime.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => ClockTime.Text = strTime);
            if (!App.Options.ShowOnlyTime)
                await
                    ClockDate.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => ClockDate.Text = strDate);

        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //For returning from options
            if (App.Options.ShowOnlyTime)
                ClockDate.Text = String.Empty;

            ClockBase.Text = App.Options.GetBaseAsString();

            m_clock.StartTimer(); 
        }

        private void ConfigureButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof (ConfigurePage));
        }

        private void Page_Tapped(object sender, TappedRoutedEventArgs e)
        {
            ConfigureButton.Visibility=Visibility.Visible;
            m_timer.Start();
        }

    }
}
