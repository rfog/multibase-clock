﻿using BinHexClockCommon;
using Windows.Storage;

namespace BinHexClockW8
{
    class Settings: LocalSettings
    {
        public override void SetValue(string name, object value)
        {
            if(ApplicationData.Current.LocalSettings.Values.Keys.Contains(name))
                ApplicationData.Current.LocalSettings.Values[name] = value;
            else
                ApplicationData.Current.LocalSettings.Values.Add(name,value);
        }

        public override object GetValue(string name, object defaultValue)
        {
            if (ApplicationData.Current.LocalSettings.Values.ContainsKey(name))
                return ApplicationData.Current.LocalSettings.Values[name];
            return defaultValue;
        }
    }
}
