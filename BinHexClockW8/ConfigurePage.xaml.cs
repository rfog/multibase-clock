﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BinHexClockCommon;
using BinHexClockW8.Common;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace BinHexClockW8
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class ConfigurePage
    {
        public ConfigurePage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        private void pageRoot_Loaded(object sender, RoutedEventArgs e)
        {
            ShowTimeCombo.SelectedIndex = (int)App.Options.ShowMinutesOrSeconds;
            ShowBaseCombo.SelectedIndex = (int) App.Options.ShowBase;

            ShowOnlyTimeCombo.SelectedIndex = App.Options.ShowOnlyTime ? 1 : 0;
        }

        private void pageRoot_Unloaded(object sender, RoutedEventArgs e)
        {
            App.Options.ShowMinutesOrSeconds = (ShowMinutesOrSeconds) ShowTimeCombo.SelectedIndex;
            App.Options.ShowBase = (ShowBase) ShowBaseCombo.SelectedIndex;
            App.Options.ShowOnlyTime = ShowOnlyTimeCombo.SelectedIndex != 0;

            App.Options.Save();
        }

        private void ShowTimeCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            App.Options.ShowMinutesOrSeconds = (ShowMinutesOrSeconds)ShowTimeCombo.SelectedIndex;
        }

        private void ShowBaseCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            App.Options.ShowBase = (ShowBase)ShowBaseCombo.SelectedIndex;
        }

        private void ShowOnlyTimeCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            App.Options.ShowOnlyTime = ShowOnlyTimeCombo.SelectedIndex != 0;
        }
    }
}
